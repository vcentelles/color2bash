#include <string>
#include <iostream>
#include <sstream>

#include <stdlib.h>

#include "color2bash.h"

using namespace std;


//Constructor
Color2Bash::Color2Bash(Special::Color special, 
                       Foreground::Color foreground,
                       Background::Color background):
                           special(special),
                           foreground(foreground), 
                           background(background){
                           
    this->special = special;
    this->foreground = foreground;
    this->background = background;
}

Color2Bash::Color2Bash(Foreground::Color foreground){
    this->special = Special::RESET;
    this->foreground = foreground;
    this->background = Background::RESET;
}

Color2Bash::Color2Bash(Background::Color background){
    this->special = Special::RESET;
    this->foreground = Foreground::RESET;
    this->background = background;
}

Color2Bash::Color2Bash(Special::Color special, Background::Color background){
    this->special = special;
    this->foreground = Foreground::RESET;
    this->background = background;
}

Color2Bash::Color2Bash(Foreground::Color foreground, Background::Color background){
    this->special = Special::RESET;
    this->foreground = foreground;
    this->background = background;
}

//Copy constructor
Color2Bash::Color2Bash(const Color2Bash &c2b){
    if (this != &c2b){
        this->special = c2b.special;
        this->foreground = c2b.foreground;
        this->background = c2b.background;
    }
}

///////////////////////////////////////////////////////////////////////////////

//Assigment operator
void Color2Bash::operator=(const Color2Bash &c2b){
    if (this != &c2b){
        this->special = c2b.special;
        this->foreground = c2b.foreground;
        this->background = c2b.background;
    }
}

///////////////////////////////////////////////////////////////////////////////


void Color2Bash::set_color(Special::Color special, 
                           Foreground::Color foreground,
                           Background::Color background){
    this->special = special;
    this->foreground = foreground;
    this->background = background;
}


void Color2Bash::set_special(Special::Color special){
    this->special = special;
}


void Color2Bash::set_foreground(Foreground::Color foreground){
    this->foreground = foreground;
}


void Color2Bash::set_background(Background::Color background){
    this->background = background;
}


string Color2Bash::color(string s){
    string s_init = "\033[";
    string s_end = "\033[0m";
    
    string result;
    ostringstream convert;
    bool has_color = false;
    
    
    convert << s_init;
    if (this->special != Special::RESET){
        convert << this->special;
        has_color = true;
    }
    if (this->foreground != Foreground::RESET){
        if (has_color){
            convert << ";";
        }
        else{
            has_color = true;
        }
        convert << this->foreground;
    }

    if (this->background != Background::RESET){
        if (has_color){
            convert << ";";
        }
        convert << this->background; 
    }
    
    convert << "m" << s << s_end;
    
    result = convert.str();

    return result;
}
