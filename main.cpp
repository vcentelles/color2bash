#include <iostream>
#include "color2bash.h"
using namespace std;

int main(void){   
    
    Color2Bash info = Color2Bash(Foreground::YELLOW);
    Color2Bash success = Color2Bash(Foreground::GREEN, Background::WHITE);
    Color2Bash error = Color2Bash(Special::BLINK, Foreground::RED, Background::YELLOW);

    cout << "INFO:     " << info.color("This is a info message!!!") << endl;
    cout << "SUCCESS:  " << success.color("This is a succes message!!!") << endl;
    cout << "ERROR:    " << error.color("This is a error message!!!") << endl;
    
    //Copy constructor
    Color2Bash info2 = Color2Bash(info);
    info2.set_foreground(Foreground::MAGENTA);
    
    //Assigment operator
    Color2Bash info3 = info2;
    info3.set_foreground(Foreground::CYAN);
    
    cout << "INFO(2):  " << info2.color("This is a info(2) message!!!") << endl;
    cout << "INFO(3):  " << info3.color("This is a info(3) message!!!") << endl;
    
    //Change the colors!!!
    error.set_special(Special::RESET);
    error.set_background(Background::BLACK);
    cout << "ERROR(2): " << error.color("This is a new error(2) message!!!") << endl;
    
    return 0;    
}

