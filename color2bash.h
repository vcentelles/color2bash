#ifndef COLOR2BASH_H
#define COLOR2BASH_H


#include <string>
using namespace std; 


namespace Special{

    enum Color{
        RESET=0,
        BRIGHT=1,
        DIM=2,
        UNDERSCORE=4,   
        BLINK=5,
        REVERSE=7,
        HIDDEN=8,
    };
};

namespace Foreground{
    
    enum Color{
        RESET=0,
        BLACK=30, 
        RED=31,
        GREEN=32,
        YELLOW=33,
        BLUE=34,
        MAGENTA=35,
        CYAN=36,
        WHITE=37,
    };
};

namespace Background{
    
    enum Color{
        RESET=0,
        BLACK=40,
        RED=41,
        GREEN=42,
        YELLOW=43,
        BLUE=44,
        MAGENTA=45,
        CYAN=46,
        WHITE=47,
    };
};

class Color2Bash{
    public:
        
        ///////////////////////////////////////////////////////////////////////
        // Constructors
        ///////////////////////////////////////////////////////////////////////
        Color2Bash(Special::Color special=Special::RESET, 
                   Foreground::Color foreground=Foreground::RESET,
                   Background::Color background=Background::RESET);
        
        Color2Bash(Foreground::Color foreground);
        Color2Bash(Background::Color background);
        Color2Bash(Special::Color special, Background::Color background);
        Color2Bash(Foreground::Color foreground, Background::Color background);
        
        //Copy constructor
        Color2Bash(const Color2Bash &c2b);
        
        ///////////////////////////////////////////////////////////////////////
        
        //Assigment operator
        void operator=(const Color2Bash &c2b);
        
        ///////////////////////////////////////////////////////////////////////
        
        //Change all the property (by default, reset the properties)
        void set_color(Special::Color special=Special::RESET, 
                       Foreground::Color foreground=Foreground::RESET,
                       Background::Color background=Background::RESET);
        
        //Change the special text property (blink, hidden, etc.)
        void set_special(Special::Color special=Special::RESET);
        
        //Change the foreground color
        void set_foreground(Foreground::Color foreground=Foreground::RESET);
        
        //Change the background color
        void set_background(Background::Color background=Background::RESET);
        
        //Apply the color properties to the string s
        string color(string s);
    
    private:
        Special::Color special;
        Foreground::Color foreground;
        Background::Color background;
};

#endif
